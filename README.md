## Installing the dot files 
* Install `stow`
```bash
# Centos
$ yum install epel-release
$ yum install -y stow
$ yum install -y git vim
```
* Install dot files
```bash
$ git clone https://gitlab.com/anishkthomas/dotfiles.git
$ cd dotfiles
$ stow *
```