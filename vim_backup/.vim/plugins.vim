call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdtree'
Plug 'Yggdroot/indentLine'
Plug 'vimwiki/vimwiki'
call plug#end()
